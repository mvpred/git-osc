---
layout: post
title: "码云 Pull Requests 多人代码审核功能上线"
---

<p>继码云刚刚推出的 <a href="https://www.oschina.net/news/99583/gitee-pull-requests-support-auto-sonar-analyse" target="_blank">Pull Request 自动分析代码质量功能</a>&nbsp;后，我们未曾停下前进的脚步。</p><p>为了满足企业团队协作开发的灵活性要求，码云的 Pull Requests 多人审核功能上线了。项目负责人可以专门指定某几个项目成员作为代码的审核人员，当其他的开发人员提交 Pull Requests 时，需要所指定的人员的某一个或者是全数通过审核，该代码方可合并到主仓库。</p><p>项目的 PR 默认是由 PR 提交者指定审核人员，项目负责人可以在项目的<span style="color:#e74c3c"><strong> “管理-&gt;代码审核设置” </strong></span>里开启多人审核以及多人测试的功能，如下图所示：</p><p><img src="https://oscimg.oschina.net/oscnet/be5b4f67cab1135cd7d2bcc3a2b8db7845f.jpg" width="800" height="459"/></p><p>项目负责人可以选择多人审核的策略，包括任何一个人审核通过都可以合并，还是需要所有人审核通过才可以合并。</p><p>此功能对所有的仓库开放，欢迎使用&nbsp;<a href="https://gitee.com?from=prnews" target="_blank">https://gitee.com</a></p>