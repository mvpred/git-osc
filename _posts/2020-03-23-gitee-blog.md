---
layout: post
title: "Gitee 代码搜索上线了，居然还可以搜索这些东西"
---

<p>截至今天，Gitee 上已经有了近千万个代码仓库，其中包含了大量开源的，可以被开发者们复用的代码资源。当开发者们在编程过程中遇到不熟悉的特定库、不知道该调用哪些函数，或者准备实现一段简单的代码时，往往会去搜索期望的代码片段， 并进行不同方式的复用。</p>

<p>Gitee 的<strong>代码搜索服务 Gitee Search </strong>便是针对这个需求推出的。</p>

<p>目前 Gitee Search 已支持通过 PC和手机网页版对全站公开非 Fork 仓库默认分支进行代码行搜索，同时也支持指定代码仓库的搜索。</p>

<p>如果想了解某个 API 其他人如何使用，可以通过代码搜索去查找指定关键字的内容。&nbsp;</p>

<p><img alt="" height="527" src="https://oscimg.oschina.net/oscnet/up-d92637309b782d85d6934e5cdafda654ba0.png" width="700" /></p>

<p>除此之外，Gitee Search 还支持开源仓库、Issue 和相关博客的搜索。</p>

<p>偷偷告诉你个秘笈：可以检查公司内部的代码有没有被泄露哦~</p>

<p>前往 <a href="https://search.gitee.com/" target="_blank">https://search.gitee.com/</a> 体验。</p>
