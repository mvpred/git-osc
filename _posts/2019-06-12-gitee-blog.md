---
layout: post
title: "实名吐槽码云企业版集成度不高，使用体验不一致"
---

<p>码云的用户群中关于码云企业版集成度不高的、使用体验不一致的吐槽，一直不绝于耳，特别是企业版中的仓库页面需要跳到企业外打开的问题。后来可能大家吐啊吐啊，也就习惯了。但是我们坚决不能忍受这样的吐槽，已经解决了不少吐槽的人，开个玩笑，别当真。</p>

<p>所以年初的时候我们就把企业版集成仓库页面的改造列为今年重点任务之一。仓库页面是整个码云系统最为复杂也是功能点最为繁多的部分，它同时还承担着开源项目和社区交互的重担。</p>

<p>我们花了很大精力准备了几个月的时间，现在终于迎来了码云六周年系列更新的第三弹 &mdash;&mdash; 企业版代码仓库页面集成。</p>

<p>码云 6 周年的其他更新系列：</p>

<ol>
	<li><a href="https://www.oschina.net/news/107029/gitee-document-updated">码云六周年系列更新第一弹 &mdash;&mdash; 企业知识库管理改版</a></li>
	<li><a href="https://www.oschina.net/news/107041/gitee-readonly-feature">码云六周年系列更新第二弹 &mdash;&mdash; Git 只读文件支持</a></li>
</ol>

<p>企业版全面集成代码仓库页面。如下图所示：</p>

<p>1. 企业版仓库的概览页面</p>

<p><img alt="" height="607" src="https://oscimg.oschina.net/oscnet/2aeb6b3cb27f71703fd5d9720b8a78fe607.jpg" width="1024" /></p>

<p>2. 企业版仓库的代码浏览页面</p>

<p><img alt="" height="595" src="https://oscimg.oschina.net/oscnet/47690233fec89a5cc976df57bd8b14212db.jpg" width="1024" /></p>

<p>这次更新我们重写了企业版代码仓库页面，将原有独立的仓库页面的所有功能集成到企业工作台内，用户的所有关于企业内的操作都可以在企业工作台内完成。</p>

<p>同时我们依然保留了独立的仓库页面，特别是企业的开源仓库。</p>

<p>由于牵扯仓库操作的功能点特别多，目前该更新还处于公测阶段，尚有个别功能点没有完全集成（如仓库的任务和 PR）。</p>

<p>欢迎大家体验。使用过程中有任何问题欢迎随时吐槽，我们会及时予以处理。</p>

<p><strong>码云企业版体验地址：<a href="https://gitee.com/enterprises?from=e-r-news">https://gitee.com/enterprises</a></strong></p>
