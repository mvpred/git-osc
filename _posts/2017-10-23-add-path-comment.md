---
layout: post
title: "码云新增文件评论功能"
---

码云一直以来仅支持 CommitDiff 的评论功能，并不支持针对某个文件或者文件夹进行评论的功能，现在码云增加了对文件以及文件夹评论的功能，并且会在评论上附加分支信息，告诉浏览者这个评论是在评论哪个版本的文件。

### 文件评论

![输入图片说明](https://gitee.com/uploads/images/2017/1023/205302_fdb6ed6f_62561.png "屏幕截图.png")

### 文件夹评论

![输入图片说明](https://gitee.com/uploads/images/2017/1023/205435_f25fa4a3_62561.png "屏幕截图.png")

赶快来试试吧 -> [https://gitee.com](https://gitee.com)
