---
layout: post
title: "码云 Gitee 组织全新改版 —— 欢迎国内开源组织入驻"
---

<p>为了更全面、丰富地展示开源组织，码云 Gitee&nbsp;对组织页面进行了全面改版。</p>

<p><strong>1. 全新组织首页风格</strong></p>

<p>我们对组织的首页进行了美化调整，<strong>新增「概览」页</strong>。「概览」除展示精选项目之外，增加组织介绍模块，右侧组织成就、仓库语言、成员一目了然，帮助开发者迅速了解组织。</p>

<p><img alt="产品更新|码云 Gitee 组织页面全新改版-码云 Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2019/10/%E7%A0%81%E4%BA%91%E7%A4%BE%E5%8C%BA%E7%89%88%E6%9B%B4%E6%96%B01.png" /></p>

<p>原有组织新增开启「概览」引导页，新建的组织默认开启「概览」。</p>

<p><strong>2. 以 Readme 的方式展示组织介绍</strong></p>

<p>组织设置中<strong>可编写更丰富的组织介绍</strong>，<strong>支持多语言版本自动切换</strong>。更详细展示组织风采。组织介绍可选择手动编写，或选择关联组织内仓库的某一个文件，指定后支持 git 命令更新和版本管理。</p>

<p><img alt="产品更新|码云 Gitee 组织页面全新改版-码云 Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2019/10/%E7%A0%81%E4%BA%91%E7%A4%BE%E5%8C%BA%E7%89%88%E6%9B%B4%E6%96%B02.png" /></p>

<p><strong>3. 组织动态筛选</strong></p>

<p>组织动态<strong>支持按仓库/成员过滤</strong>，轻松聚焦你想看的最新动态。</p>

<p><img alt="产品更新|码云 Gitee 组织页面全新改版-码云 Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2019/10/%E7%A0%81%E4%BA%91%E7%A4%BE%E5%8C%BA%E7%89%88%E6%9B%B4%E6%96%B03.png" /></p>

<p><strong>4.开源组织展示</strong></p>

<p>码云Gitee开源软件页<strong>新增知名组织 LOGO 入口</strong>，顶尖开源组织项目一键直达。</p>

<p><img alt="产品更新|码云 Gitee 组织页面全新改版-码云 Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2019/10/%E7%A0%81%E4%BA%91%E7%A4%BE%E5%8C%BA%E7%89%88%E6%9B%B4%E6%96%B04.png" /></p>

<p>&nbsp;</p>

<p><strong>码云 Gitee 开源组织：</strong></p>

<ul>
	<li>腾讯蓝鲸智云：<a href="https://gitee.com/Tencent-BlueKing" target="_blank">https://gitee.com/Tencent-BlueKing</a></li>
	<li>滴滴开源：<a href="https://gitee.com/didiopensource" target="_blank">https://gitee.com/didiopensource</a></li>
	<li>华为鸿蒙：<a href="https://gitee.com/harmonyos" target="_blank">https://gitee.com/harmonyos</a></li>
	<li>华为欧拉&nbsp;<a href="https://gitee.com/openeuler">https://gitee.com/openeuler</a></li>
	<li><a href="https://gitee.com/pkuvcl">https://gitee.com/pkuvcl</a>&nbsp;&nbsp;北京大学数字视频编解码技术国家工程实验室视频编码组</li>
	<li>腾讯微信读书开发团队&nbsp;<a href="https://gitee.com/QMUI">https://gitee.com/QMUI</a></li>
	<li>蚂蚁金服 SOFASTACK：<a href="https://gitee.com/sofastack" target="_blank">https://gitee.com/sofastack</a></li>
	<li>腾讯TARS：<a href="https://gitee.com/tarscloud" target="_blank">https://gitee.com/tarscloud</a></li>
	<li>WeBank微众银行：<a href="https://gitee.com/webank" target="_blank">https://gitee.com/webank</a></li>
</ul>

<p>欢迎更多开源组织入驻<a href="http://gitee.com/" target="_blank">码云 Gitee</a>&nbsp;，与众多开发者一同完善开源项目，分享开源成果。</p>
