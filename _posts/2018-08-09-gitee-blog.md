---
layout: post
title: "码云企业版客户上线了限制仓库强制推送的功能"
---

<p>Git 有一个很可怕的特性，就是 -f 参数 —— 强制推送，强行用本地仓库覆盖远端仓库。导致的后果就是文件可能被老的内容给覆盖掉，仓库的历史提交记录丢失等等。</p><p>进行强制推送一般是出现在仓库版本冲突的情况下，在团队协同开发中，很多开发者为了省事，直接不负责任的使用 -f 推送，悲剧就这样发生了。</p><p>正常的流程就是想办法解决冲突，再行推送。</p><p>可是谁身边又没有几个猪队友呢？</p><p>----- 以下是广告时间 -----</p><p>我们刚为码云企业版客户上线了限制仓库强制推送的功能，从仓库的管理页面中，可以选择“禁止强制推送”。</p><p><img alt="" height="187" src="https://oscimg.oschina.net/oscnet/3f25a2cae5216b01be7226c7ca8d3b27b41.jpg" width="400"/></p><p>一旦禁用了强制推送后，如果开发者在推送冲突时使用 -f 参数，就会报如下错误：</p><p><span style="color:#c0392b"><strong>denying non-fast-forward refs/heads/master (you should pull first)</strong></span></p><p><img alt="" height="208" src="https://oscimg.oschina.net/oscnet/cd6aa1d2755cb10e9a6dbd95068973c7e27.jpg" width="619"/></p><p>该功能目前只对企业客户开放、默认不启用，需要手工开启。</p><p>当然我们更建议大家使用 fork + pull requests 进行协作开发，或者设置主分支只读，然后通过 pull requests 进行代码合并。</p><p>即刻前往体验码云企业版&nbsp;<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>